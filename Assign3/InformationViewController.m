//
//  InformationViewController.m
//  Assign3
//
//  Created by Trevor Eckhardt on 5/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InformationViewController.h"

@interface InformationViewController () : ViewController
@end

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end