//
//  ViewController.h
//  Assign3
//
//  Created by Trevor Eckhardt on 5/26/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) NSArray* array;

@end

