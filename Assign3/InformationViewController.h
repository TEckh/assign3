//
//  InformationViewController.h
//  Assign3
//
//  Created by Trevor Eckhardt on 5/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#ifndef Assign3_InformationViewController_h
#define Assign3_InformationViewController_h


#endif

@interface InformationViewController : UIViewController


@property (nonatomic, strong) NSDictionary* informationDictionary;

@end
