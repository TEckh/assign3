//
//  ViewController.m
//  Assign3
//
//  Created by Trevor Eckhardt on 5/26/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@class InformationViewController;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:self.view.bounds style: UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    NSArray* array = [[NSArray alloc] initWithContentsOfFile:filePath];
    
    NSDictionary* ChickDump = [array objectAtIndex:0];
    NSDictionary* Enchil = [array objectAtIndex:1];
    NSDictionary* Tapioca = [array objectAtIndex:2];
    NSDictionary* Pizza = [array objectAtIndex:3];
    NSDictionary* BBQ = [array objectAtIndex:4];
    NSLog(@"here");
    
    //sets defaults
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"My string" forKey:@"myString"];
    [defaults synchronize];
    NSLog(@"%@", [defaults stringForKey:@"myString"]);
}
    
- (NSInteger)tableView:(UITableView*) tableView numberOfRowsInSection:(NSInteger)section{
    return [self.array count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* infoDictionary = [self.array objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [infoDictionary objectForKey:@"name"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* infoDictionary = self.array[indexPath.row];
    
    InformationViewController* info = new InformationViewController;
    info.informationDictionary = infoDictionary;
    [self.navigationController pushViewController:info animated:YES];
}

-(void)btnTouched:(UIButton*)btn {
    InformationViewController* info = new InformationViewController;
    [self.navigationController pushViewController:info animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
